'use strict';
/*
- ki kene cserelni a ROOMS-t redisre
*/
require('dotenv').config()
const express=require('express')
const ip = require('ip')
const bodyParser = require('body-parser')
const app=express()
app.use(bodyParser.json())
const cors=require('cors')
app.use(cors())
const server = require('http').Server(app)
const io = require('socket.io')(server, { 
    path:'/wss',
})

const ROOMS = {}
const TOOFAST=5 // lehetne percenkent is, de ez is ok

const {API_URL, PORT}=process.env
const axios=require('axios')
const moment=require('moment')
server.listen(PORT, ()=> console.log(`WS server is running on ${ip.address()}:${PORT}`) )

const router=express.Router()
router.get('/test', (req, res)=> res.sendFile(__dirname + '/test.html') )
router.get('/rooms/:room_key/members', (req, res)=> res.json( ROOMS[req.params.room_key] ) )
router.get('/rooms', (req, res)=>{
    const rms={}
    for(let i in ROOMS){
        rms[ i ] = Object.keys( ROOMS[i].clients ).length
    }
    return res.json( rms )
})

app.use('/wss_rest', router)

// auth
io.use(async(socket, next)=>{

    const { 
        room_key, 
        user_ukey, 
        user_id, 
        token
    } = socket.handshake.query
    
    console.debug( 'Room_key, user_ukey, user_id, token', room_key, user_ukey, user_id, token )
    
    try{
        if(!room_key){
            throw new Error('Missing room key/user_ukey')
        }
        const client=axios.create({
            baseURL: API_URL,
            headers:{
                authorization: token,
            },
        })    
        const getUserRequest=token ? `/user/me` : `/users/${user_ukey}`
        
        const [ user_resp, room_resp ]=await Promise.all([
            client.get( getUserRequest ),
            client.get(`/chat/${room_key}`)
        ])
        if(!user_resp||!room_resp){
            throw new Error('Unknown user/room')
        }
        //const user=user_resp.data
        const _user=user_resp.data        
        const user={
            user_id: _user.user_id,
            user_avatar: _user.user_avatar,
            user_unique_name: _user.user_unique_name,
            user_tags: _user.user_tags,
            user_name: _user.user_name,
            user_ukey: _user.user_ukey,
            user_ugroup: _user.user_ugroup,
        }

        const isbannedresponse=await client.get('/chat/isbanned', { params: { user_id: user.user_id, room_key } })
        if(isbannedresponse.status!=200){
            throw new Error('Persona non grata error')
        }

        const room=room_resp.data
        console.log(`Socket #${socket.id} :: User #${user.user_id} @ ${room.room_key} connected `)
        socket.send(socket.id)

        //socket.user=user
        socket.user=user

        socket.room=room
        socket.axios=client

        if ( !ROOMS[ socket.room.room_key ] ){
            console.log('New room: ', room_key)
            ROOMS[room_key]={
                clients: {},
                removeClient(user_id){
                    delete this.clients[user_id]
                }
            }
        }
        if( ROOMS[room_key].clients[ user.user_id ] ){
            throw new Error('Do not exist in so many instances thanks')
        }

        ROOMS[room_key].clients[user.user_id]=user
        socket.join( room_key )
        return next()
    } catch(err){
        if(ROOMS[room_key]){
            // ezert tudott masodjara ujracsatlakozni, omfg
            //ROOMS[room_key].removeClient(user_id)
        }
        console.error('Got error:', err.toString() )
        socket.disconnect(true)
    }
})

io.on('connection', async(socket) => {
    console.debug(`[${socket.id}]  User_id ${socket.user.user_id} joined room '${socket.room.room_key}'`)

    socket.in( socket.room.room_key ).emit('refresh')

    socket.emit('hello', socket.user )

    socket.on('error', err => {
        console.debug( err )
        if(socket.room&&socket.user&&ROOMS[socket.room.room_key]){
            ROOMS[socket.room.room_key].removeClient(socket.user.user_id)
        }
        socket.in( socket.room.room_key ).emit('refresh')
        socket.disconnect(true)
    })

    socket.on('ban', async(data) => {
        const {user_id, room_key}=data
        console.debug('Kicking user', user_id, 'from room', room_key)
        if(socket.user.user_ugroup<1){
            console.debug('User.user_ugroup error', socket.user)
            return
        }
        await socket.axios.post('/chat/ban', { user_id, room_key })
        io.in(room_key).emit('ban', data)
    })

    socket.on('typing', what => {
        const { room, user } = socket
        //const action_id= socket.user.user_id + '-' + (new Date()/10)
        io.in(room.room_key).emit('typing', socket.user)
    })

    // szoveges uzenetek kezelese es tovabbitasa
    socket.on('new_message', async(data) => {
        const ts=moment().unix()
        const { message, image } = data
        const { room, user, lastmsgtime } = socket
        if( lastmsgtime&&lastmsgtime>ts-TOOFAST ){
            return 
        }

        console.log(`(@${ts})[${user.user_id}->${room.room_key}]: "${message}"`)
        try {
            await socket.axios.post(`/chat/${room.room_key}/messages/frominternal`, { 
                owner: socket.user.user_id,
                message,
                image, 
            })
            io.in(room.room_key).emit('new_message', { 
                message, 
                ...user,
                room,
                ts,
            })
            
            socket.lastmsgtime=ts

        } catch(err){
            console.log( err.toString() )
        }
    })

    socket.on('disconnect', async()=> {        
        if(socket.room&&socket.user&&ROOMS[socket.room.room_key]){
            ROOMS[socket.room.room_key].removeClient(socket.user.user_id)
        }
        socket.disconnect(true)
        socket.in( socket.room.room_key ).emit('refresh')
        console.debug(`Goodbye ${socket.id} from #${socket.room.room_key}`)
    })
})
